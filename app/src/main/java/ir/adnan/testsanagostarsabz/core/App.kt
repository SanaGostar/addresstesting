package ir.adnan.testsanagostarsabz.core


import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.cedarstudios.cedarmapssdk.CedarMaps
import ir.adnan.testsanagostarsabz.BuildConfig

class App : MultiDexApplication() {

    override protected fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)

    }

    override fun onCreate() {
        super.onCreate()

        initMap()
    }

    fun initMap() {

        CedarMaps.getInstance()
            .setClientID(clientId)
            .setClientSecret(clientSecret)
            .setContext(this)
    }

    companion object {

        //CedarMaps
        var clientId = "nilsoo-16368063827389515212"
        var clientSecret = "b6KYMm5pbHNvbx1-G0RQ7duJOMOveRYUBT5DeRL-xM2dwV1M7f3h_9QQ"


        @JvmStatic
        fun getBaseUrl(): String {
            val baseUrl: String
            if (BuildConfig.BUILD_TYPE.contains("debug"))
                baseUrl = Const.API_URL_DEFAULT
            else
                baseUrl = Const.API_URL_DEFAULT

            return baseUrl
        }
    }
}