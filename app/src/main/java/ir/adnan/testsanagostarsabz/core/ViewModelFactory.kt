package ir.adnan.testsanagostarsabz.core

import android.annotation.SuppressLint
import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.annotation.VisibleForTesting
import ir.adnan.testsanagostarsabz.repository.Repository
import ir.adnan.testsanagostarsabz.ui.MainActivityViewModel
import ir.adnan.testsanagostarsabz.ui.fragment.list.ListViewModel
import ir.adnan.testsanagostarsabz.ui.fragment.map.MapViewModel
import ir.adnan.testsanagostarsabz.ui.fragment.sign.SignViewModel

open class ViewModelFactory(
        private val mApplication: Application, private val mRepository: Repository)
    : ViewModelProvider.NewInstanceFactory() {


    override fun <T : ViewModel> create(modelClass: Class<T>): T {

        //Activity
        if (modelClass.isAssignableFrom(MainActivityViewModel::class.java))
            return MainActivityViewModel(mApplication, mRepository) as T

        //Fragment
        else if (modelClass.isAssignableFrom(SignViewModel::class.java))
            return SignViewModel(mApplication, mRepository) as T
        else if (modelClass.isAssignableFrom(ListViewModel::class.java))
            return ListViewModel(mApplication, mRepository) as T
        else if (modelClass.isAssignableFrom(MapViewModel::class.java))
            return MapViewModel(mApplication, mRepository) as T

        throw IllegalArgumentException("Unknown ViewModel class: " + modelClass.name)
    }

    companion object {

        @SuppressLint("StaticFieldLeak")
        @Volatile
        private var INSTANCE: ViewModelFactory? = null

        fun getInstance(application: Application, repository: Repository): ViewModelFactory? {

            if (INSTANCE == null) {
                synchronized(ViewModelFactory::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = ViewModelFactory(application,
                                repository)
                    }
                }
            }
            return INSTANCE
        }

        @VisibleForTesting
        fun destroyInstance() {
            INSTANCE = null
        }
    }
}
