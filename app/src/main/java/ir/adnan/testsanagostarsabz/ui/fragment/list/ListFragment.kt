package ir.adnan.testsanagostarsabz.ui.fragment.list

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ir.adnan.testsanagostarsabz.core.ViewModelFactory
import ir.adnan.testsanagostarsabz.core.ui.CoreFragment
import ir.adnan.testsanagostarsabz.databinding.FragmentListBinding
import ir.adnan.testsanagostarsabz.repository.Repository
import ir.adnan.testsanagostarsabz.ui.MainActivity
import ir.adnan.testsanagostarsabz.ui.fragment.list.viewmodel.AddressItem
import ir.adnan.testsanagostarsabz.util.DialogUtils
import ir.adnan.testsanagostarsabz.util.action.Act1
import ir.adnan.testsanagostarsabz.util.action.Act2


class ListFragment : CoreFragment() {

    val TAG = "ListFragment"

    lateinit var binding: FragmentListBinding
    lateinit var viewModel: ListViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return init(inflater, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecyclerView()
        initToolbar()
        observer()

        list()
    }

    fun init(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = FragmentListBinding.inflate(inflater, container, false)

        viewModel = obtainViewModel(this!!.activity!!)

        return binding.root
    }

    fun initToolbar() {
        try {
            (activity as MainActivity).changeTitleToolbar("لیست آدرس")
        } catch (e: Exception) {
        }
    }

    fun initRecyclerView() {

        binding.addressRecycler.layoutManager = LinearLayoutManager(
            context,
            RecyclerView.VERTICAL, false
        )

        binding.addressRecycler.adapter = viewModel?.adapter
    }

    fun list() {
        Log.e(TAG , "list method")
        viewModel.getKarfarmaAddress()
    }

    fun observer() {
        viewModel.listResponseResponseLiveEventLoading.observe(this , Observer {
            DialogUtils.getInstance()!!.showDialogByReceiveData(context!!, it, object : DialogUtils.Callbacks {
                override fun onPositiveSelected(index: Int) {

                    list()
                }

                override fun onNegativeSelected(index: Int) {
                    activity!!.onBackPressed()
                }

                override fun onInputEntered(enteredCode: String) {
                }
            })
        })

        viewModel.listResponseLiveEvent.observe(this , Observer {
            viewModel.adapter.removeAll()

            for (address in it) {
                viewModel.adapter.addItem(AddressItem(address , Act2 { lat, lng ->  }))
            }
        })
    }

    fun obtainViewModel(activity: FragmentActivity): ListViewModel {
        val viewModelFactory =
            ViewModelFactory.getInstance(
                activity.getApplication(),
                Repository.getInstance(activity.getApplication())
            )

        return ViewModelProviders.of(activity, viewModelFactory).get(ListViewModel::class.java!!)
    }
}