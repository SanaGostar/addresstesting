package ir.adnan.testsanagostarsabz.ui.fragment.list

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LifecycleObserver
import ir.adnan.testsanagostarsabz.core.SingleLiveEvent
import ir.adnan.testsanagostarsabz.model.response.AddressResponse
import ir.adnan.testsanagostarsabz.repository.Repository
import ir.adnan.testsanagostarsabz.repository.remote.DataSource
import ir.adnan.testsanagostarsabz.util.view.LoadingType
import ir.coderz.ghostadapter.GhostAdapter
import okhttp3.ResponseBody

class ListViewModel(application: Application, val repository: Repository) : AndroidViewModel(application),
    LifecycleObserver {

    val listResponseLiveEvent = SingleLiveEvent<List<AddressResponse>>()
    var listResponseResponseLiveEventLoading = SingleLiveEvent<LoadingType>()

    val adapter = GhostAdapter()

    fun getKarfarmaAddress() {
        if (NetworkUtils.instance.isOnline(getApplication())) {
            listResponseResponseLiveEventLoading.value = LoadingType.SHOW_LOADING

            repository.remoteRepository.getKarfarmasAddress(object : DataSource.GetKarfarmasAddressCallback {
                override fun onSuccess(r: List<AddressResponse>) {
                    listResponseResponseLiveEventLoading.value = LoadingType.HIDE_LOADING

                    var result = r
                    listResponseLiveEvent.value = result

                }

                override fun onFailed(f: Throwable?) {
                    listResponseResponseLiveEventLoading.value = LoadingType.HIDE_LOADING

                }

            })
        } else {
            listResponseResponseLiveEventLoading.value = LoadingType.RETRY
        }
    }
}