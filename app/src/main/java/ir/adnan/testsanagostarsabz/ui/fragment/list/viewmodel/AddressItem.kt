package ir.adnan.testsanagostarsabz.ui.fragment.list.viewmodel

import ir.adnan.testsanagostarsabz.R
import ir.adnan.testsanagostarsabz.model.response.AddressResponse
import ir.adnan.testsanagostarsabz.util.action.Act1
import ir.adnan.testsanagostarsabz.util.action.Act2
import ir.coderz.ghostadapter.BindItem
import ir.coderz.ghostadapter.Binder
import kotlinx.android.synthetic.main.item_address.view.*

@BindItem(layout = R.layout.item_address, holder = AddressHolder::class, binding = true)
class AddressItem(val response : AddressResponse ,
                  val onClickListener: Act2<Double, Double>
) {
    @Binder
    public fun bind(holder: AddressHolder) {
        holder.binding.address.text = response.address
        holder.binding.name.setText(response.first_name + " " +response.last_name)
        holder.binding.mobile.text = response.coordinate_mobile
        holder.itemView.setOnClickListener {
            onClickListener.call(response.lat , response.lng)
        }

    }
}