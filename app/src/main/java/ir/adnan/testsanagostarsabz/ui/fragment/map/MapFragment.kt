package ir.adnan.testsanagostarsabz.ui.fragment.map

import android.graphics.PointF
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.cedarstudios.cedarmapssdk.CedarMaps
import com.cedarstudios.cedarmapssdk.listeners.OnTilesConfigured
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import ir.adnan.testsanagostarsabz.core.ViewModelFactory
import ir.adnan.testsanagostarsabz.core.ui.CoreFragment
import ir.adnan.testsanagostarsabz.databinding.FragmentMapBinding
import ir.adnan.testsanagostarsabz.repository.Repository
import ir.adnan.testsanagostarsabz.ui.MainActivity
import ir.adnan.testsanagostarsabz.ui.fragment.map.pojo.MapType

class MapFragment : CoreFragment() {

    private var type: String? = null

    lateinit var binding: FragmentMapBinding
    lateinit var viewModel: MapViewModel


    companion object {
        val TAG = "MarketPlaceCategoryF"
        const val TYPE = "bundle_id"

        fun newInstance(type: Int): MapFragment {
            var mapFragment = MapFragment()
            var bundle = Bundle()
            bundle.putInt(TYPE, type)
            mapFragment!!.arguments = bundle
            return mapFragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        initCedar()
        init(inflater, container)
        initToolbar()
        initArgument()
        initMap()

        observer()
        onClick()
        return binding.root
    }

    private fun init(inflater: LayoutInflater, container: ViewGroup?) {
        binding = FragmentMapBinding.inflate(inflater, container, false)

        viewModel = obtainViewModel(this!!.activity!!)

    }

    private fun initArgument() {
        if (arguments != null) {
            viewModel?.type = arguments!!.getInt(MapFragment.TYPE, 0)
        }
    }

    fun initToolbar() {
        try {
            (activity as MainActivity).changeTitleToolbar("نقشه")
        } catch (e: Exception) {
        }
    }

    fun obtainViewModel(activity: FragmentActivity): MapViewModel {
        val viewModelFactory =
            ViewModelFactory.getInstance(
                activity.getApplication(),
                Repository.getInstance(activity.getApplication())
            )

        return ViewModelProviders.of(activity, viewModelFactory).get(MapViewModel::class.java)
    }

    fun initCedar() {
        CedarMaps.getInstance().prepareTiles(object : OnTilesConfigured {
            override fun onSuccess() {
            }

            override fun onFailure(@NonNull error: String) {
            }
        })
    }

    fun initMap() {
        binding.mapView.getMapAsync(OnMapReadyCallback {
            it.setMaxZoomPreference(17.0)
            it.setMinZoomPreference(6.0)
            viewModel.mapBoxMap = it
            viewModel.mapInit(activity!!)
        })

    }

    fun observer() {
        viewModel.showSingleLiveEventReverse.observe(this, Observer {

            var address = ""
            if(it.province!!.isNotEmpty()) {
                address += it.province
            }
            if(it.city!!.isNotEmpty()) {
                address+= (" , "+it.city)
            }
            if(it.district!!.isNotEmpty()) {
                address+=(" , "+it.district)
            }
            if(it.locality!!.isNotEmpty()) {
                address+=(" , "+it.locality)
            }
            if(it.address!!.isNotEmpty()) {
                address+=(" , "+it.address)
            }
            Log.e(
                TAG,
                "Address : " + it.province + " - " + it.city + " - " + it.district + " - " + it.locality + " - " + it.address
            )

            (activity as MainActivity).sendData(address , viewModel.selectedLatLong)

            activity!!.onBackPressed()
        })

        viewModel.showSingleLiveEventLoading.observe(this, Observer {

        })
    }

    fun onClick() {

        binding.mapMarker.setOnClickListener(View.OnClickListener {

            viewModel.selectedLatLong = getLatLonSelected()

            if (viewModel.selectedLatLong != null) {
                viewModel.getReverseAddressByLocation(viewModel.selectedLatLong!!)
            }

        })

        binding.cardGps.setOnClickListener(View.OnClickListener {

            try {
                if (viewModel.currentLocation != null)
                    viewModel.chengeCameraMap(
                        viewModel.currentLocation!!.latitude,
                        viewModel.currentLocation!!.longitude
                    )
            } catch (e: Exception) {
            }
        })

        binding.nextButton.setOnClickListener {
            try {
                binding.mapMarker.performClick()
            } catch (e : java.lang.Exception) {}
        }
    }

    fun getLatLonSelected(): LatLng? {

        val centreX = (binding.layoutMap.getX() + binding.layoutMap.getWidth() / 2)
        val centreY =
            (binding.layoutMap.getY() + binding.layoutMap.getHeight() / 2 + binding.mapMarker.getHeight() / 2)

        val x_y_points = PointF(centreX, centreY)

        return viewModel.mapBoxMap!!.getProjection().fromScreenLocation(x_y_points)
        return null
    }


}
