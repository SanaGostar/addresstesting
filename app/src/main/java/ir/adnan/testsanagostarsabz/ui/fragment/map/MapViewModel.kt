package ir.adnan.testsanagostarsabz.ui.fragment.map

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.content.pm.PackageManager
import android.location.Location
import android.util.Log
import androidx.core.app.ActivityCompat

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LifecycleObserver
import com.cedarstudios.cedarmapssdk.CedarMaps
import com.cedarstudios.cedarmapssdk.listeners.ReverseGeocodeResultListener
import com.cedarstudios.cedarmapssdk.model.geocoder.reverse.ReverseGeocode
import com.mapbox.android.core.location.LocationEngine
import com.mapbox.android.core.location.LocationEngineListener
import com.mapbox.android.core.location.LocationEnginePriority
import com.mapbox.android.core.location.LocationEngineProvider
import com.mapbox.mapboxsdk.annotations.Icon
import com.mapbox.mapboxsdk.annotations.IconFactory
import com.mapbox.mapboxsdk.annotations.Marker
import com.mapbox.mapboxsdk.annotations.MarkerOptions
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapboxMap
import ir.adnan.testsanagostarsabz.R
import ir.adnan.testsanagostarsabz.core.SingleLiveEvent
import ir.adnan.testsanagostarsabz.repository.Repository
import ir.adnan.testsanagostarsabz.util.view.LoadingType


class MapViewModel(internal val application: Application,
                   val repository: Repository
) : AndroidViewModel(application), LifecycleObserver {

    //    var mapirMap: MapirMap? = null
    var type = 0
    var currentLocation: Location? = null
    var lastLocation: LatLng? = null
    var SecondLocation: LatLng? = null

    var isSLastLocation = false
    var isSecondLocation = false
    lateinit var mapBoxMap: MapboxMap
    var currentLocationMarker: Marker? = null
    var markerList = ArrayList<Marker>()

    var selectedLatLong : LatLng? = null

    var showSingleLiveEventLoading = SingleLiveEvent<LoadingType>()
    var showSingleLiveEventReverse = SingleLiveEvent<ReverseGeocode>()

    fun mapInit(activity: Activity) {


        if (ActivityCompat.checkSelfPermission(
                        activity!!,
                        Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
        )
            ActivityCompat.requestPermissions(
                    activity!!, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1
            )
        if (ActivityCompat.checkSelfPermission(
                        activity!!,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
        )
            ActivityCompat.requestPermissions(
                    activity!!, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), 1
            )
//
        mapBoxMap.getLocationComponent().activateLocationComponent(activity)
        mapBoxMap.getLocationComponent().setLocationComponentEnabled(true)
        mapBoxMap.uiSettings.isRotateGesturesEnabled = false
//
        var locationEngine = LocationEngineProvider(activity).obtainLocationEngineBy(LocationEngine.Type.ANDROID);
        locationEngine!!.setPriority(LocationEnginePriority.BALANCED_POWER_ACCURACY);
        locationEngine!!.addLocationEngineListener(object : LocationEngineListener {


            @SuppressLint("MissingPermission")
            override fun onConnected() {
                locationEngine.requestLocationUpdates()
            }


            override fun onLocationChanged(location: Location?) {
                Log.d("DDD", "")

                currentLocation = location
                chengeCameraMap(location!!.latitude, location!!.longitude)
//                setCurrentLocation(activity)
                locationEngine.removeLocationEngineListener(this)
            }
        })
        locationEngine.activate()

//
    }

    fun setCurrentLocation(activity: Activity) {

        var latLng = LatLng(currentLocation!!.latitude, currentLocation!!.longitude)

        addMarker(latLng, activity, true)


    }


    fun chengeCameraMap(lat: Double, lon: Double) {

        mapBoxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat, lon), 20.0));
    }

    fun addMarker(latLng: LatLng, activity: Activity, isCurrentLocaion: Boolean) {
        var icon: Icon? = null
        var iconFactory = IconFactory.getInstance(activity)
//        if (isCurrentLocaion) {

//            if (currentLocationMarker != null) {
//                mapBoxMap!!.removeMarker(currentLocationMarker!!)
//            } else {
//
//
//            }

//            icon = iconFactory.fromResource(R.mipmap.current_location)


//        } else {
        icon = iconFactory.fromResource(R.mipmap.location_pin)
//
        if (lastLocation == null) {
            lastLocation = latLng
            isSLastLocation = true
            chengeCameraMap(lastLocation!!.latitude - 0.0005, lastLocation!!.longitude)
        } else {

            SecondLocation = latLng
            isSecondLocation = true
        }
//        }
        val markerOptions = MarkerOptions().position(latLng).title("").icon(icon)
//        if (isCurrentLocaion) {
//            currentLocationMarker = mapBoxMap!!.addMarker(markerOptions)
//        } else {
        markerList.add(mapBoxMap!!.addMarker(markerOptions))
//        }


    }


    fun removeMarker() {

        if (!markerList.isNullOrEmpty()) {
            val marker = markerList.get(markerList.lastIndex)
            mapBoxMap!!.removeMarker(marker)
            markerList.removeAt(markerList.lastIndex)
        }
    }

    fun getReverseAddressByLocation(latLng: LatLng) {

        if (NetworkUtils.instance.isOnline(getApplication())) {

            showSingleLiveEventLoading.value = LoadingType.SHOW_LOADING

            CedarMaps.getInstance().reverseGeocode(latLng, object : ReverseGeocodeResultListener {
                override fun onSuccess(result: ReverseGeocode) {
                    showSingleLiveEventReverse.value = result
                    showSingleLiveEventLoading.value = LoadingType.HIDE_LOADING


                }

                override fun onFailure(errorMessage: String) {
                    showSingleLiveEventLoading.value = LoadingType.RETRY

                }

            })
        } else {
            showSingleLiveEventLoading.value = LoadingType.RETRY

        }
    }

}