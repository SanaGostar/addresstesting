package ir.adnan.testsanagostarsabz.ui.fragment.map.pojo

/**
 * Created by Adnan Abdollah Zaki on 4/20/19.
 * adnan9011@gmail.com
 */
enum class MapType(var value: Int) {
    ONE_LOCATION(1);

    companion object {
        fun from(findValue: Int): MapType = MapType.values().first { it.value == findValue }
    }
}