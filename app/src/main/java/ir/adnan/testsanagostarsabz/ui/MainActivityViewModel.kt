package ir.adnan.testsanagostarsabz.ui

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LifecycleObserver
import ir.adnan.testsanagostarsabz.repository.Repository
import ir.adnan.testsanagostarsabz.repository.remote.RemoteRepository

class MainActivityViewModel(application: Application ,
                            val repository: Repository) : AndroidViewModel(application) , LifecycleObserver {

    val TAG = "MainActivityVM"


}