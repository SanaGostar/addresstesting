package ir.adnan.testsanagostarsabz.ui.fragment.sign

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LifecycleObserver
import com.mapbox.mapboxsdk.geometry.LatLng
import ir.adnan.testsanagostarsabz.core.SingleLiveEvent
import ir.adnan.testsanagostarsabz.model.request.KarfarmasAddressRequest
import ir.adnan.testsanagostarsabz.model.response.AddressResponse
import ir.adnan.testsanagostarsabz.repository.Repository
import ir.adnan.testsanagostarsabz.repository.remote.DataSource
import ir.adnan.testsanagostarsabz.util.view.LoadingType

class SignViewModel(application: Application, val repository: Repository) : AndroidViewModel(application),
    LifecycleObserver {

    val TAG = "SignViewModel"

    val signResponseLiveEvent = SingleLiveEvent<AddressResponse>()
    var signResponseResponseLiveEventLoading = SingleLiveEvent<LoadingType>()

    var isGendarWomenEnable = true
    var isDialogAddressShow = false

    var selectedLatLong : LatLng? = null

    var address = ""
    var coordinate_mobile = ""
    var coordinate_phone_number = ""
    var first_name = ""
    var gender = ""
    var last_name = ""
    var lat = 0.0
    var long = 0.0

    fun postKarfarmaAddress(address : String ,
                            coordinate_mobile : String ,
                            coordinate_phone_number : String ,
                            first_name : String ,
                            gender : String ,
                            last_name : String ,
                            lat : Double ,
                            long : Double ) {

        if (NetworkUtils.instance.isOnline(getApplication())) {
            signResponseResponseLiveEventLoading.value = LoadingType.SHOW_LOADING

            repository.remoteRepository.postKarfarmasAddress(KarfarmasAddressRequest(address,coordinate_mobile,
                coordinate_phone_number,first_name,
                gender,last_name,
                lat,long,1),
                object : DataSource.PostKarfarmasAddressCallback {

                    override fun onSuccess(r: AddressResponse?) {

                        signResponseResponseLiveEventLoading.value = LoadingType.HIDE_LOADING

                        var result = r
                        signResponseLiveEvent.value = result

                        Log.e(TAG , "onSuccess : ")
                    }

                    override fun onFailed(f: Throwable?) {
                        signResponseResponseLiveEventLoading.value = LoadingType.RETRY

                        Log.e(TAG , "onFailed : ")
                    }
                })
        } else {
            signResponseResponseLiveEventLoading.value = LoadingType.RETRY
        }

    }
}