package ir.adnan.testsanagostarsabz.ui.fragment.sign

import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.mapbox.mapboxsdk.geometry.LatLng
import ir.adnan.testsanagostarsabz.R
import ir.adnan.testsanagostarsabz.core.ViewModelFactory
import ir.adnan.testsanagostarsabz.core.ui.CoreFragment
import ir.adnan.testsanagostarsabz.databinding.FragmentSignBinding
import ir.adnan.testsanagostarsabz.repository.Repository
import ir.adnan.testsanagostarsabz.ui.MainActivity
import ir.adnan.testsanagostarsabz.util.DialogUtils

class SignFragment : CoreFragment() {

    val TAG = "SignFragment"

    lateinit var binding: FragmentSignBinding
    lateinit var viewModel: SignViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.e(TAG, "onCreateView")

        return init(inflater, container)

    }

    override fun onResume() {
        super.onResume()

        Log.e(TAG, "onResume")

        initToolbar()

        if (viewModel.isDialogAddressShow) {
            binding.contentAddress.editText.isFocusableInTouchMode = true
            binding.contentAddress.editText.isFocusable = true
            binding.contentAddress.editText.isEnabled = true
            binding.contentAddress.editText.inputType =
                InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_MULTI_LINE
            binding.contentAddress.editText.requestFocus()

            binding.contentName.editText.setText("")
            binding.contentFamili.editText.setText("")
            binding.contentTelephone.editText.setText("")
            binding.contentMobile.editText.setText("")
            binding.contentAddress.editText.setText("")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Log.e(TAG, "onViewCreated")


        initEditText()
        initGender()
        initAddress()

        observer()
    }

    fun init(inflater: LayoutInflater, container: ViewGroup?): View {
        binding = FragmentSignBinding.inflate(inflater, container, false)

        viewModel = obtainViewModel(this!!.activity!!)

        return binding.root
    }

    fun initToolbar() {
        try {
            (activity as MainActivity).changeTitleToolbar("ثبت نام")
        } catch (e: Exception) {
        }
    }

    fun initEditText() {
        binding.contentName.title.text = "نام"
        binding.contentName.editText.setText(viewModel.first_name)
        binding.contentName.editText.inputType = InputType.TYPE_CLASS_TEXT
        binding.contentName.editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s != null && s.toString().length > 2) {
                    binding.contentName.imageView.setBackgroundResource(R.drawable.ic_tick_selected_checkbox)
                } else {
                    binding.contentName.imageView.setBackgroundResource(R.drawable.ic_filled_circle)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        binding.contentFamili.title.text = "نام خانوادگی"
        binding.contentFamili.editText.setText(viewModel.last_name)
        binding.contentFamili.editText.inputType = InputType.TYPE_CLASS_TEXT
        binding.contentFamili.editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s != null && s.toString().length > 2) {
                    binding.contentFamili.imageView.setBackgroundResource(R.drawable.ic_tick_selected_checkbox)
                } else {
                    binding.contentFamili.imageView.setBackgroundResource(R.drawable.ic_filled_circle)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        binding.contentMobile.title.text = "تلفن همراه"
        binding.contentMobile.editText.setText(viewModel.coordinate_mobile)
        binding.contentMobile.editText.inputType = InputType.TYPE_CLASS_PHONE
        binding.contentMobile.editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s != null && s.toString().length == 11 && s.toString().startsWith("09")) {
                    binding.contentMobile.imageView.setBackgroundResource(R.drawable.ic_tick_selected_checkbox)
                } else {
                    binding.contentMobile.imageView.setBackgroundResource(R.drawable.ic_filled_circle)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        binding.contentTelephone.title.text = "تلفن ثابت"
        binding.contentTelephone.editText.setText(viewModel.coordinate_phone_number)
        binding.contentTelephone.editText.inputType = InputType.TYPE_CLASS_PHONE
        binding.contentTelephone.editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s != null && s.toString().length == 11 && s.toString().startsWith("0")) {
                    binding.contentTelephone.imageView.setBackgroundResource(R.drawable.ic_tick_selected_checkbox)
                } else {
                    binding.contentTelephone.imageView.setBackgroundResource(R.drawable.ic_filled_circle)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        binding.contentAddress.title.text = "آدرس دقیق"
        binding.contentAddress.editText.inputType =
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_MULTI_LINE
        binding.contentAddress.editText.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION)
        binding.contentAddress.editText.setSingleLine(false)
        binding.contentAddress.editText.maxLines = 4
        binding.contentAddress.editText.setLines(4)
        binding.contentAddress.editText.minLines = 4
        binding.contentAddress.editText.isFocusableInTouchMode = false
        binding.contentAddress.editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s != null && s.toString().length > 19) {
                    binding.contentAddress.imageView.setBackgroundResource(R.drawable.ic_tick_selected_checkbox)
                } else {
                    binding.contentAddress.imageView.setBackgroundResource(R.drawable.ic_filled_circle)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

    }

    fun initGender() {
        binding.gendarMen.setOnClickListener {
            onClickGender(womenEnable = false)
        }

        binding.gendarWomen.setOnClickListener {
            onClickGender(womenEnable = true)
        }

        binding.nextButton.setOnClickListener {
            sign()
        }
    }

    fun initAddress() {
        binding.contentAddress.editText.setOnClickListener {

            binding.contentAddress.editText.isFocusableInTouchMode = true
            binding.contentAddress.editText.isFocusable = true
            binding.contentAddress.editText.isEnabled = true
            binding.contentAddress.editText.inputType =
                InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_MULTI_LINE


            if (!viewModel.isDialogAddressShow) {

                DialogUtils.getInstance()!!.showChooseMessage(
                    requireContext(),
                    object : DialogUtils.Callbacks {
                        override fun onPositiveSelected(index: Int) {
                            Log.e(TAG, "onPositiveSelected")
                            (activity as MainActivity).mapFragment()
                        }

                        override fun onNegativeSelected(index: Int) {
                            Log.e(TAG, "onNegativeSelected")
                        }

                        override fun onInputEntered(enteredCode: String) {
                        }

                    })

                viewModel.isDialogAddressShow = true

            } else {

            }
        }
    }

    fun observer() {
        viewModel?.signResponseResponseLiveEventLoading!!.observe(this, Observer {
            DialogUtils.getInstance()!!.showDialogByReceiveData(context!!, it, object : DialogUtils.Callbacks {
                override fun onPositiveSelected(index: Int) {

                    sign()
                }

                override fun onNegativeSelected(index: Int) {

                    activity!!.onBackPressed()
                }

                override fun onInputEntered(enteredCode: String) {
                }
            })
        })

        viewModel?.signResponseLiveEvent.observe(this , Observer {
            DialogUtils.getInstance()!!.showErrorMessage(requireContext() , "اطلاعات دریافت شده :\n"+it.toString() , "" , 0 , object : DialogUtils.Callbacks {
                override fun onPositiveSelected(index: Int) {

                    viewModel.address = ""
                    viewModel.coordinate_mobile = ""
                    viewModel.coordinate_phone_number = ""
                    viewModel.first_name = ""
                    viewModel.last_name = ""

                    (activity as MainActivity).listFragment()
                }

                override fun onNegativeSelected(index: Int) {
                }

                override fun onInputEntered(enteredCode: String) {
                }
            })
        })
    }

    fun onClickGender(womenEnable: Boolean) {
        if (womenEnable) {
            viewModel.isGendarWomenEnable = true

            binding.gendarWomen.setBackgroundColor(resources.getColor(R.color.colorBlue))
            binding.gendarMen.setBackgroundColor(resources.getColor(R.color.colorWhite))

            binding.gendarWomen.setTextColor(resources.getColor(R.color.colorWhite))
            binding.gendarMen.setTextColor(resources.getColor(R.color.colorBlue))
        } else {
            viewModel.isGendarWomenEnable = false

            binding.gendarMen.setBackgroundColor(resources.getColor(R.color.colorBlue))
            binding.gendarWomen.setBackgroundColor(resources.getColor(R.color.colorWhite))

            binding.gendarMen.setTextColor(resources.getColor(R.color.colorWhite))
            binding.gendarWomen.setTextColor(resources.getColor(R.color.colorBlue))
        }
    }

    fun sign() {


        if (binding.contentName.editText.text.isNullOrEmpty()) {
            DialogUtils.getInstance()!!.showErrorMessage(
                requireContext(),
                "نام خالی است",
                "",
                0,
                null
            )
            return
        } else if (binding.contentName!!.editText.text!!.length < 3) {
            DialogUtils.getInstance()!!.showErrorMessage(
                requireContext(),
                "نام حداقل سه حرفی می باشد",
                "",
                0,
                null
            )
            return
        } else {
            viewModel.first_name = binding.contentName.editText.getText().toString()
        }

        if (binding.contentFamili.editText.text.isNullOrEmpty()) {
            DialogUtils.getInstance()!!.showErrorMessage(
                requireContext(),
                "نام خانوادگی خالی است",
                "",
                0,
                null
            )
            return
        } else if (binding.contentFamili!!.editText.text!!.length < 3) {
            DialogUtils.getInstance()!!.showErrorMessage(
                requireContext(),
                "نام خانوادگی حداقل سه حرفی می باشد",
                "",
                0,
                null
            )
            return
        } else {
            viewModel.last_name = binding.contentFamili.editText.getText().toString()
        }

        if (binding.contentMobile.editText.text.isNullOrEmpty()) {
            DialogUtils.getInstance()!!.showErrorMessage(
                requireContext(),
                "شماره موبایل خالی است",
                "",
                0,
                null
            )
            return
        } else if (binding.contentMobile!!.editText.text!!.length != 11) {
            DialogUtils.getInstance()!!.showErrorMessage(
                requireContext(),
                "شماره موبایل 11 عددی می باشد",
                "",
                0,
                null
            )
            return
        } else if (!binding.contentMobile!!.editText.text!!.startsWith("09")) {
            DialogUtils.getInstance()!!.showErrorMessage(
                requireContext(),
                "شماره موبایل با 09 شروع می شود",
                "",
                0,
                null
            )
            return
        } else {
            viewModel.coordinate_mobile = binding.contentMobile.editText.getText().toString()
        }

        if (binding.contentTelephone.editText.text.isNullOrEmpty()) {
            DialogUtils.getInstance()!!.showErrorMessage(
                requireContext(),
                "شماره تماس خالی است",
                "",
                0,
                null
            )
            return
        } else if (binding.contentTelephone!!.editText.text!!.length != 11) {
            DialogUtils.getInstance()!!.showErrorMessage(
                requireContext(),
                "شماره تماس 11 عددی می باشد",
                "",
                0,
                null
            )
            return
        } else if (!binding.contentTelephone!!.editText.text!!.startsWith("0")) {
            DialogUtils.getInstance()!!.showErrorMessage(
                requireContext(),
                "شماره تلفن با 0 شروع می شود",
                "",
                0,
                null
            )
            return
        } else {
            viewModel.coordinate_phone_number = binding.contentTelephone.editText.getText().toString()
        }

        if (binding.contentAddress.editText.text.isNullOrEmpty()) {
            DialogUtils.getInstance()!!.showErrorMessage(
                requireContext(),
                "آدرس خالی است",
                "",
                0,
                null
            )
            return
        } else if (binding.contentAddress!!.editText.text!!.length < 20) {
            DialogUtils.getInstance()!!.showErrorMessage(
                requireContext(),
                "آدرس حداقل بیست حرفی می باشد",
                "",
                0,
                null
            )
            return
        } else {
            viewModel.address = binding.contentAddress.editText.getText().toString()
        }

        if (viewModel.isGendarWomenEnable) {
            viewModel.gender = "Female"
        } else {
            viewModel.gender = "Male"
        }

        if (viewModel.selectedLatLong != null) {
            viewModel.lat = viewModel.selectedLatLong!!.latitude
            viewModel.long = viewModel.selectedLatLong!!.longitude
        }


        viewModel.postKarfarmaAddress(
            viewModel.address,
            viewModel.coordinate_mobile,
            viewModel.coordinate_phone_number,
            viewModel.first_name,
            viewModel.gender,
            viewModel.last_name,
            viewModel.lat,
            viewModel.long
        )
    }

    fun sendData(address: String, selectedLatLong: LatLng?) {
        Log.e(TAG, "sendData , address : " + address)
        Log.e(TAG, "sendData , latLong : " + selectedLatLong.toString())

        Handler().postDelayed(Runnable {
            viewModel.selectedLatLong = selectedLatLong
            binding.contentAddress.editText.setText(address)
        }, 1000)
    }

    fun obtainViewModel(activity: FragmentActivity): SignViewModel {
        val viewModelFactory =
            ViewModelFactory.getInstance(
                activity.getApplication(),
                Repository.getInstance(activity.getApplication())
            )

        return ViewModelProviders.of(activity, viewModelFactory).get(SignViewModel::class.java!!)
    }

}