package ir.adnan.testsanagostarsabz.ui.fragment.list.viewmodel

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import ir.adnan.testsanagostarsabz.databinding.ItemAddressBinding

class AddressHolder : RecyclerView.ViewHolder {
    var binding: ItemAddressBinding

    constructor(binding: ViewDataBinding) : super(binding.root) {
        this.binding = binding as ItemAddressBinding
    }
}