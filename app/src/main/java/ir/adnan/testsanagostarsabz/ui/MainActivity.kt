package ir.adnan.testsanagostarsabz.ui

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.mapbox.mapboxsdk.geometry.LatLng
import ir.adnan.testsanagostarsabz.R
import ir.adnan.testsanagostarsabz.core.ViewModelFactory
import ir.adnan.testsanagostarsabz.core.ui.CoreActivity
import ir.adnan.testsanagostarsabz.databinding.ActivityMainBinding
import ir.adnan.testsanagostarsabz.repository.Repository
import ir.adnan.testsanagostarsabz.ui.fragment.list.ListFragment
import ir.adnan.testsanagostarsabz.ui.fragment.map.MapFragment
import ir.adnan.testsanagostarsabz.ui.fragment.map.pojo.MapType
import ir.adnan.testsanagostarsabz.ui.fragment.sign.SignFragment

class MainActivity : CoreActivity() {

    val TAG = "MainActivity"

    lateinit var binding: ActivityMainBinding
    lateinit var viewModel: MainActivityViewModel

    var signFragment: SignFragment? = null
    var mapFragment: MapFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        init()

        signFragment(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
    }

    private fun init() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel = obtainViewModel(this)
    }

    private fun signFragment(savedInstanceState: Bundle?) {
        signFragment = SignFragment()
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .add(R.id.container, signFragment!!)
                .commitNow()
        }
    }

    fun mapFragment() {
        mapFragment = MapFragment.newInstance(MapType.ONE_LOCATION.value)
        addFragmentSimple(mapFragment!!, R.id.container)
    }

    fun listFragment() {
        replaceFragmentSimple(ListFragment(), R.id.container)
    }

    fun addFragmentSimple(fragment: MapFragment, fragmentId: Int) {

        var fragmentTransaction = supportFragmentManager!!.beginTransaction()
        fragmentTransaction
            .setCustomAnimations(
                R.animator.enter_from_left,
                R.animator.enter_from_right,
                R.animator.pop_to_left,
                R.animator.pop_to_right
            )
            .add(fragmentId, fragment)
            .addToBackStack(fragment.javaClass.simpleName)
            .commit()
    }

    fun replaceFragmentSimple(fragment: Fragment, fragmentId: Int) {

        var fragmentTransaction = supportFragmentManager!!.beginTransaction()
        fragmentTransaction
            .setCustomAnimations(
                R.animator.enter_from_left,
                R.animator.enter_from_right,
                R.animator.pop_to_left,
                R.animator.pop_to_right
            )
            .replace(fragmentId, fragment)
            .addToBackStack(fragment.javaClass.simpleName)
            .commit()
    }

    fun changeTitleToolbar(titleTest: String) {
        binding.contentToolbar.toolbarTitle.text = titleTest
    }

    fun obtainViewModel(activity: MainActivity): MainActivityViewModel {
        val viewModelFactory = ViewModelFactory.getInstance(
            activity.getApplication(),
            Repository.getInstance(activity.getApplication())
        )

        return ViewModelProviders.of(activity, viewModelFactory)
            .get(MainActivityViewModel::class.java)
    }

    fun sendData(address: String, selectedLatLong: LatLng?) {
        if (signFragment != null)
            signFragment!!.sendData(address, selectedLatLong)
    }


}
