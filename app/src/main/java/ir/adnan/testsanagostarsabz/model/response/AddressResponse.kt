package ir.adnan.testsanagostarsabz.model.response

data class AddressResponse(
    val address: String,
    val coordinate_mobile: String,
    val coordinate_phone_number: String,
    val first_name: String,
    val gender: String,
    val id: Int,
    val last_name: String,
    val lat: Double,
    val lng: Double,
    val region: Region
) {
    data class Region(
        val city_object: CityObject,
        val id: Int,
        val name: String,
        val state_object: StateObject
    ) {
        data class CityObject(
            val city_id: Int,
            val city_name: String
        )

        data class StateObject(
            val state_id: Int,
            val state_name: String
        )
    }
}