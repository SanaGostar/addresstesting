
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.util.DisplayMetrics
import android.view.WindowManager
import android.net.Uri
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.view.accessibility.AccessibilityEventCompat.setAction
import androidx.fragment.app.FragmentActivity
import android.util.Patterns
import java.util.*


public class Utils private constructor() {

    companion object {
        val instance: Utils
            get() = Utils.InsanceHolder.sInstance
    }

    object InsanceHolder {
        val sInstance = Utils()
    }

    internal fun getDisplayMetrics(context: Context): DisplayMetrics {
        var displayMetrics = DisplayMetrics()

        var windowmanager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager

        windowmanager?.getDefaultDisplay().getMetrics(displayMetrics)

        return displayMetrics
    }

    internal fun getWidthOfDeviceInDP(context: Context): Int {

        var width = getDisplayMetrics(context).widthPixels
        return (0.671875 * width).toInt()
    }

    internal fun getWidthOfDeviceOneToOneInDP(context: Context): Int {

        var width = getDisplayMetrics(context).widthPixels

        return width
    }

    internal fun getWidthOfDeviceTwoToOneInDP(context: Context): Int {

        var width = getDisplayMetrics(context).widthPixels

        return (0.5 * width).toInt()
    }

    internal fun getWidthOfDeviceThreePointOneToOneInDP(context: Context): Int {

        var width = getDisplayMetrics(context).widthPixels

        return (0.3139 * width).toInt()
    }

    internal fun getWidthOfDeviceFourToOneInDP(context: Context): Int {

        var width = getDisplayMetrics(context).widthPixels

        return (0.25 * width).toInt()
    }

    internal fun getWidthOfDeviceThreeToOneInDP(context: Context): Int {

        var width = getDisplayMetrics(context).widthPixels

        return (0.3375 * width).toInt()
    }

    internal fun geHightOfDeviceTwoToOneInDPMian(context: Context, margin: Int): Int {

        var width = getDisplayMetrics(context).widthPixels
        var hight = ((width / 1.91) - (margin * 3)) / 2


        return hight.toInt()
    }

    internal fun geHightOfDeviceTwoToOneInDPDoubleBanner(context: Context, margin: Int): Int {

        var width = getDisplayMetrics(context).widthPixels
        var hight = ((width / 2.15) - (margin * 3)) / 2


        return hight.toInt()
    }

    fun thousentPrice(string: String): String {
        var s: String? = null
        try {
            // The comma in the format specifier does the trick
            s = String.format("%,d", java.lang.Long.parseLong(string))
        } catch (e: NumberFormatException) {
        }

        return s!!
    }

    fun shareText(context: Context, text: String) {
        val sendIntent = Intent()
        sendIntent.setAction(Intent.ACTION_SEND)
        sendIntent.putExtra(Intent.EXTRA_TEXT, text)
        sendIntent.setType("text/plain")
        context.startActivity(sendIntent)
    }

    fun intentBrowser(context: Context, url: String) {
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        context.startActivity(i)
    }

    fun intentEmail(context: Context, emailAddress: String) {
        val emailIntent = Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", emailAddress, null))
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "موضوع ...")
        emailIntent.putExtra(Intent.EXTRA_TEXT, "از دستگاه اندروید ...")
        context.startActivity(Intent.createChooser(emailIntent, "ارسال ایمیل به نیلسو"))
    }

    fun intentDial(context: Context, phone: String) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:" + phone)
        context.startActivity(intent)
    }

    fun intentTelegram(context: Context, telegramID: String) {
        val telegram = Intent(Intent.ACTION_VIEW, Uri.parse("https://telegram.me/" + telegramID))
        context.startActivity(telegram)
    }

    fun intentMap(context: Context, latLon: List<Double>) {
        val uri = String.format(Locale.ENGLISH, "geo:%f,%f", latLon[0], latLon[1])
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
        context.startActivity(intent)
    }

    fun hideKeyboard(activity: Activity) {
//        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//        //Find the currently focused view, so we can grab the correct window token from it.
//        var view = (context as Activity).currentFocus
//        //If no view currently has focus, create a new one, just so we can grab a window token from it
//        if (view == null) {
//            view = View(context)
//        }
//        imm.hideSoftInputFromWindow(view!!.windowToken, 0)

        val view = activity.findViewById<View>(android.R.id.content)
        if (view != null) {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view!!.getWindowToken(), 0)
        }
    }

    fun showKeyboard(context: Context, et: AppCompatEditText) {
        et.requestFocus()
        et.postDelayed(Runnable {
            val keyboard = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
            keyboard!!.showSoftInput(et, 0)
        }, 200)
    }

    fun setTimerForPrice(context: FragmentActivity, oldPrice: Long, price: Long, textView: TextView) {
        var old = oldPrice


        Thread(Runnable {

            if (old < price) {
                var tempPrice = 0
                var step = 10
                while (tempPrice < price) {

                    old += step
                    step += step

                    context.runOnUiThread {
                        textView.text = Utils.instance.thousentPrice("$old") + " تومان"
                    }

                    if (old >= price) {
                        old = price

                        context.runOnUiThread {
                            textView.text = Utils.instance.thousentPrice("$price") + " تومان"
                        }
                        break
                    }

                    Thread.sleep(100)
                }
            } else {
                var step = -10
                var temp = -20
                while (old > price) {
                    old += step
                    step += temp
                    temp += temp
                    Log.d("oldPrice", "$old")
                    context.runOnUiThread {
                        textView.text = Utils.instance.thousentPrice("$old") + " تومان"
                    }

                    if (old <= price) {
                        old = price
                        context.runOnUiThread {
                            textView.text = Utils.instance.thousentPrice("$price") + " تومان"
                        }

                    }


                    Thread.sleep(100)
                }

            }


        }).start()

    }

    fun getVersionApp(context: Context): Int {
        try {
            val pInfo = context.packageManager.getPackageInfo(context.getPackageName(), 0)
            val version = pInfo.versionCode
            return version
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
            return 0
        }

    }

    fun isValidEmail(target: CharSequence): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }
}
