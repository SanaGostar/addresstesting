package ir.adnan.testsanagostarsabz.util.view

/**
 * Created by Adnan Abdollah Zaki on 4/20/19.
 * adnan9011@gmail.com
 */
enum class LoadingType(var value: Int) {
    SHOW_LOADING(1),
    HIDE_LOADING(2),
    RETRY(3),
    NO_INTERNET(4);

    companion object {
        fun from(findValue: Int): LoadingType = LoadingType.values().first { it.value == findValue }
    }
}