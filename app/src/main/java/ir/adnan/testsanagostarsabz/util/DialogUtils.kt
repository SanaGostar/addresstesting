package ir.adnan.testsanagostarsabz.util

import android.content.Context

import android.app.Dialog
import android.app.FragmentManager
import android.content.DialogInterface
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText
import androidx.databinding.Observable
import androidx.databinding.ObservableField
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SortedList
import ir.adnan.testsanagostarsabz.R
import ir.adnan.testsanagostarsabz.util.view.LoadingDialog
import ir.adnan.testsanagostarsabz.util.view.LoadingType
import ir.arash.lottietest.dialog.FaildToConnectNetDialog
import ir.coderz.ghostadapter.GhostAdapter
import java.util.*
import kotlin.collections.RandomAccess

class DialogUtils private constructor() {

    val TAG = "DialogUtils"
    private var loadingDialog: LoadingDialog? = null
    private var isLodingShowing = false
    private var faildToConnectNetDialog: FaildToConnectNetDialog? = null

    fun showErrorMessage(context: Context, message: String, negativeTitle: String,
                         code: Int, callbacks: DialogUtils.Callbacks?) {
        var resources = context.getResources()
        showMessage(context,
                "",
                message,
                resources.getString(R.string.action_dialog_ok),
                negativeTitle,
                false,
                true,
                code,
                callbacks)
    }

    fun showChooseMessage(context: Context, callbacks: DialogUtils.Callbacks?) {
        var resources = context.getResources()
        showMessage(context,
            "انتخاب آدرس",
            "نشانی دقیق خود را روی نقشه مشخص کنید",
            resources.getString(R.string.action_dialog_ok),
            "",
            false,
            false,
            Codes.NOT_NEEDED,
            callbacks)
    }

    fun showDialogByReceiveData(context: Context, status: LoadingType, callbacks: DialogUtils.Callbacks?) {

        when (status) {

            LoadingType.SHOW_LOADING -> {

                if (!isLodingShowing) {

                    Log.e(TAG, "SHOW_LOADING")

                    loadingDialog = LoadingDialog(context)
                    loadingDialog!!.showDialog()

                    isLodingShowing = true
                }
            }

            LoadingType.HIDE_LOADING -> {
                if (isLodingShowing) {
                    loadingDialog!!.dismissDialog()
                    loadingDialog = null

                    isLodingShowing = false
                }
            }

            LoadingType.RETRY -> {
                Log.e("LOADING", "RETRY")

                faildToConnectNetDialog = FaildToConnectNetDialog(context)
                faildToConnectNetDialog!!.callbacks = callbacks
//                if (loadingDialog!=null)
                loadingDialog?.dismissDialog()
                loadingDialog = null
                faildToConnectNetDialog!!.showDialog()

                isLodingShowing = false
            }

            LoadingType.NO_INTERNET -> {
                Log.e("LOADING", "NO INTERNET")

                faildToConnectNetDialog = FaildToConnectNetDialog(context)
                faildToConnectNetDialog!!.callbacks = callbacks
//                if (loadingDialog!=null)
                loadingDialog?.dismissDialog()
                loadingDialog = null
                faildToConnectNetDialog!!.showDialog()

                isLodingShowing = false
            }
        }
    }

    private fun showMessage(context: Context,
                            title: CharSequence?,
                            message: CharSequence?,
                            positiveButton: String?,
                            negativeButton: String?,
                            isCancelable: Boolean,
                            autoDismiss: Boolean,
                            code: Int,
                            callback: DialogUtils.Callbacks?) {

        val dialog = Dialog(context, R.style.CustomDialogTheme)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(isCancelable)

        dialog.setContentView(R.layout.item_dialog_accept_cancel)

        dialog.findViewById<TextView>(R.id.message).setText(message)

        if (positiveButton!!.isNullOrEmpty()) {
            dialog.findViewById<AppCompatButton>(R.id.accept).visibility = View.GONE
        } else {
            dialog.findViewById<AppCompatButton>(R.id.accept).text = positiveButton
            dialog.findViewById<AppCompatButton>(R.id.accept).setOnClickListener(
                    object : View.OnClickListener {
                        override fun onClick(v: View?) {

                            callback?.onPositiveSelected(code)
                            dialog.dismiss()

                        }
                    })
        }
        //
        if (negativeButton!!.isNullOrEmpty()) {
            dialog.findViewById<AppCompatButton>(R.id.cancel).visibility = View.GONE
        } else {
            dialog.findViewById<AppCompatButton>(R.id.cancel).text = negativeButton
            dialog.findViewById<AppCompatButton>(R.id.cancel).setOnClickListener(
                    object : View.OnClickListener {
                        override fun onClick(v: View?) {
                            callback!!.onNegativeSelected(code)
                            dialog.dismiss()
                        }
                    })
        }

        dialog.setOnDismissListener(object : DialogInterface.OnDismissListener {
            override fun onDismiss(dialog: DialogInterface?) {
            }
        })

        dialog.show()
    }

    inline fun <T, reified R> List<T>.mapToTypedArray(transform: (T) -> R): Array<R> {
        return when (this) {
            is RandomAccess -> Array(size) { index -> transform(this[index]) }
            else -> with(iterator()) { Array(size) { transform(next()) } }
        }
    }

    interface Callbacks {
        fun onPositiveSelected(index: Int)

        fun onNegativeSelected(index: Int)

        fun onInputEntered(enteredCode: String)
    }

    object Codes {

        //        General
        val RATE_APP = 1
        //        Update Related
        val FORCE_UPDATE = 10
        val NORMAL_UPDATE = 11
        //        User Related
        val SIGN_IN_NEEDED = 20
        val MULTI_SESSION_ERROR = 21
        val CONFIRM_SIGN_OUT = 22
        //        Package Related
        val ASK_PURCHASE_PACKAGE = 30

        val NOT_NEEDED = -1
    }

    companion object {

        private var instance: DialogUtils? = null
        fun getInstance(): DialogUtils? {

            if (instance == null) {
                instance = DialogUtils()
            }

            return instance
        }
    }
}
