package ir.adnan.testsanagostarsabz.util.action;

/**
 * Created by sajad on 8/23/17.
 */
@FunctionalInterface
public interface Act2<X, Y> extends Act {
    void call(X x, Y y);
}
