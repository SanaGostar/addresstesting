package ir.adnan.testsanagostarsabz.util.action;

/**
 * Created by sajad on 8/23/17.
 */
@FunctionalInterface
public interface Act0 extends Act {
    void call();
}
