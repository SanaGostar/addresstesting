package ir.adnan.testsanagostarsabz.util.retrofit

import android.util.Log

import com.google.gson.GsonBuilder

import java.io.IOException
import java.util.concurrent.TimeUnit

import ir.adnan.testsanagostarsabz.BuildConfig
import ir.adnan.testsanagostarsabz.core.App
import ir.adnan.testsanagostarsabz.core.Const
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class UtilRetrofit private constructor() {

    private val retrofit: Retrofit

    init {

        val headerInterceptor = Interceptor { chain ->
            val builderHeader = chain.request().newBuilder()
//            builderHeader.addHeader("Platform", "Android")

            //TODO: Add user token to header
//            builderHeader.addHeader("Authorization", Preferences.getInstance())

//            //UserAgent
//            builderHeader.addHeader("User-Agent","IB232qPuO6tZYX017oE48JB8u8n3eWsh")

            //TODO: Get server time from header
            //String API_HEADER_SERVER_TIME = "ServerTime";

            val request = builderHeader.build()
            var response: Response? = null
            try {
                response = chain.proceed(request)
                if (response!!.isSuccessful) {
                    if (BuildConfig.DEBUG) {
                        if (response.cacheResponse() != null) {
                            Log.d(TAG, request.url().toString() + " intercept: cached")
                        } else {
                            Log.d(TAG, request.url().toString() + " intercept: no cache")
                        }
                    }
                }

                // else{}
            } catch (e: IOException) {
                e.printStackTrace()
            }

            response
        }
        val builder = OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)

        builder.addInterceptor(headerInterceptor)

        //Username And Password for Test
        builder.addInterceptor(BasicAuthInterceptor(Const.AuthUsername,Const.AuthPassword))


        if ( BuildConfig.DEBUG ) {
            val logging = HttpLoggingInterceptor()

            logging.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(logging)

        }

        val client = builder.build()

        val gson = GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .serializeNulls()
                .create()

        retrofit = Retrofit.Builder()
                .baseUrl(App.getBaseUrl())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(client)
                .build()
    }

    companion object {

        private val TAG = "NetWorkRepository"
        private var networkRepository: UtilRetrofit? = null

        fun <T> using(aClass: Class<T>): T {

            if (!aClass.isInterface)
                throw IllegalArgumentException("API declarations must be interfaces.")

            if (networkRepository == null)
                networkRepository =
                    UtilRetrofit()

            return networkRepository!!.retrofit.create(aClass)
        }

        fun <T> schedule(observable: Observable<T>): Observable<T> {
            return observable
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
        }
    }
}