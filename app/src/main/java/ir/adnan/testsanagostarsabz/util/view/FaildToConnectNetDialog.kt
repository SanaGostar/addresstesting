package ir.arash.lottietest.dialog

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import androidx.databinding.DataBindingUtil
import ir.adnan.testsanagostarsabz.R
import ir.adnan.testsanagostarsabz.databinding.DialogFaildToConnectNetBinding
import ir.adnan.testsanagostarsabz.util.DialogUtils

class FaildToConnectNetDialog : AlertDialog.Builder {
    var dialogFaildToConnectNetBinding: DialogFaildToConnectNetBinding? = null
    var callbacks: DialogUtils.Callbacks? = null
    lateinit var alertDialog: AlertDialog

    constructor(context: Context) : super(context) {
        initView()
    }

    fun initView() {
        dialogFaildToConnectNetBinding = DataBindingUtil.inflate(
                LayoutInflater.from(context), R.layout.dialog_faild_to_connect_net
                ,
                null,
                false
        ) as DialogFaildToConnectNetBinding
        setView(dialogFaildToConnectNetBinding!!.getRoot())
        onClicks()
        setCancelable(false)
        alertDialog = create()
        alertDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        fillLottie()
    }

    fun onClicks() {

        dialogFaildToConnectNetBinding!!.accept.setOnClickListener(View.OnClickListener {
            if (callbacks != null)
                callbacks!!.onPositiveSelected(0)
            dismissDialog()
        })

        dialogFaildToConnectNetBinding!!.cancel.setOnClickListener(View.OnClickListener {
            if (callbacks != null)
                callbacks!!.onNegativeSelected(1)
            dismissDialog()
        })

    }

    fun showDialog() {

        alertDialog.show()
    }

    fun dismissDialog() {

        alertDialog.dismiss()
    }

    fun fillLottie() {

        dialogFaildToConnectNetBinding!!.lottie.setAnimation("lottie/bouncy_loader.json")
        dialogFaildToConnectNetBinding!!.lottie.playAnimation()
        dialogFaildToConnectNetBinding!!.lottie.loop(true)
    }
}
