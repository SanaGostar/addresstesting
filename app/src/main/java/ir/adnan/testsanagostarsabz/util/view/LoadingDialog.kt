package ir.adnan.testsanagostarsabz.util.view

import android.app.AlertDialog
import android.content.Context
import android.os.Handler
import android.view.LayoutInflater

import androidx.databinding.DataBindingUtil
import ir.adnan.testsanagostarsabz.R
import ir.adnan.testsanagostarsabz.databinding.DialogLoadingBinding

class LoadingDialog : AlertDialog.Builder {
    var dialogLoadingBinding: DialogLoadingBinding? = null

    lateinit var alertDialog: AlertDialog

    constructor(context: Context) : super(context) {
        initView()
    }

    private fun initView() {
        dialogLoadingBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context), R.layout.dialog_loading,
            null,
            false
        ) as DialogLoadingBinding
        setView(dialogLoadingBinding!!.getRoot())
        setCancelable(false)
        alertDialog = create()

        alertDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        fillLottie()

    }

    fun showDialog() {

        try {
            alertDialog.show()
        } catch (e: Exception) {
        }
    }

    fun dismissDialog() {
        try {

            Handler().postDelayed(Runnable {
                alertDialog.dismiss()
            }, 0)
        } catch (e: IllegalArgumentException) {
        } catch (e: Exception) {
        }
    }

    private fun fillLottie() {

        dialogLoadingBinding!!.lottie.setAnimation("lottie/bouncy_loader.json")
        dialogLoadingBinding!!.lottie.playAnimation()
        dialogLoadingBinding!!.lottie.loop(true)
    }


}
