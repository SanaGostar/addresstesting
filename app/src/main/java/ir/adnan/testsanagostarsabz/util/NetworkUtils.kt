
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

class NetworkUtils private constructor() {

    companion object {
        val instance: NetworkUtils
            get() = InstanceHolder.sInstance
    }

    object InstanceHolder {
        val sInstance = NetworkUtils()
    }

    fun isOnline(context: Context): Boolean {
        try {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }
    }
}
