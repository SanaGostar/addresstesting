package ir.adnan.testsanagostarsabz.repository.remote

import ir.adnan.testsanagostarsabz.model.request.KarfarmasAddressRequest
import ir.adnan.testsanagostarsabz.util.retrofit.UtilRetrofit

class RemoteRepository : DataSource {
    override fun postKarfarmasAddress(
        request: KarfarmasAddressRequest,
        callback: DataSource.PostKarfarmasAddressCallback
    ) {
        UtilRetrofit.schedule(
            UtilRetrofit.using(ApiService.UserService::class.java)
                .postKarfarmasAddress(request)
        )
            .subscribe(
                callback::onSuccess,
                { t: Throwable? -> callback.onFailed(t) }
            )
    }

    override fun getKarfarmasAddress(callback: DataSource.GetKarfarmasAddressCallback) {
        UtilRetrofit.schedule(
            UtilRetrofit.using(ApiService.UserService::class.java)
                .getKarfarmasAddress()
        )
            .subscribe(
                callback::onSuccess,
                { t: Throwable? -> callback.onFailed(t) }
            )
    }
}