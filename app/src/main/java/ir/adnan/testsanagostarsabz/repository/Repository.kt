package ir.adnan.testsanagostarsabz.repository

import android.app.Application
import ir.adnan.testsanagostarsabz.repository.local.LocalRepository
import ir.adnan.testsanagostarsabz.repository.remote.RemoteRepository

class Repository(val application : Application) {
    val remoteRepository = RemoteRepository()
    val localRepository = LocalRepository()

    companion object {
        @Volatile private var instance: Repository? = null

        fun getInstance(application : Application): Repository =
            instance ?: synchronized(this) {
                instance
                    ?: createObject(application).also { instance = it }
            }

        private fun createObject (application: Application) = Repository(application)
    }
}