package ir.adnan.testsanagostarsabz.repository.remote

import ir.adnan.testsanagostarsabz.model.request.KarfarmasAddressRequest
import ir.adnan.testsanagostarsabz.model.response.AddressResponse
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import rx.Observable

interface ApiService {

    interface UserService {
        @POST("karfarmas/address")
        fun postKarfarmasAddress (@Body request : KarfarmasAddressRequest) : Observable<AddressResponse>

        @GET("karfarmas/address")
        fun getKarfarmasAddress () : Observable<List<AddressResponse>>
    }
}