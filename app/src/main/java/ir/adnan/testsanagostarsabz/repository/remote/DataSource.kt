package ir.adnan.testsanagostarsabz.repository.remote

import ir.adnan.testsanagostarsabz.model.request.KarfarmasAddressRequest
import ir.adnan.testsanagostarsabz.model.response.AddressResponse
import okhttp3.ResponseBody

interface DataSource {
    interface PostKarfarmasAddressCallback {
        fun onSuccess(r: AddressResponse?)
        fun onFailed(f: Throwable?)
    }

    fun postKarfarmasAddress(request : KarfarmasAddressRequest , callback : PostKarfarmasAddressCallback)

    interface GetKarfarmasAddressCallback {
        fun onSuccess(r: List<AddressResponse>)
        fun onFailed(f: Throwable?)
    }

    fun getKarfarmasAddress(callback : GetKarfarmasAddressCallback)
}